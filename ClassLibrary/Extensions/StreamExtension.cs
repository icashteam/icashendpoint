﻿using System.IO;
using System.Threading.Tasks;

namespace ClassLibrary.Extensions
{
    /// <summary>
    ///  Ver 1.0
    /// </summary>
    public static class StreamExtension
    {
        public static byte[] ReadToEnd(this Stream stream)
        {
            byte[] buffer = new byte[4096];
            using (MemoryStream ms = new MemoryStream())
            {
                int read = 0;
                while ((read = stream.Read(buffer, 0, buffer.Length)) > 0)
                    ms.Write(buffer, 0, read);

                return ms.ToArray();
            }
        }

        public static async Task AsyncCopyTo(this Stream from, Stream to)
        {
            byte[] buffer = new byte[4096];
            while (true)
            {
                int read = await from.ReadAsync(buffer, 0, buffer.Length);
                if (read <= 0) break;

                await to.WriteAsync(buffer, 0, read);
                //to.Write(buffer, 0, read);
            }
        }
    }
}

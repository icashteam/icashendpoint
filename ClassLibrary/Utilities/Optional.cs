﻿
namespace ClassLibrary.Utilities
{
    public class Optional<T>
    {
        public bool Valid { get; set; }
        public T Value { get; set; }
        public string ErrorMessage { get; set; }
        public int ErrorCode { get; set; }

        public static implicit operator bool(Optional<T> self)
        {
            return self.Valid;
        }

        public static Optional<T> Error(string ErrorMessage, int ErrorCode = Undefined)
        {
            return new Optional<T> { Valid = false, Value = default(T), ErrorMessage = ErrorMessage, ErrorCode = ErrorCode };
        }

        private const int Undefined = -100;
    }

    public static class OptionalExtension
    {
        public static Optional<T> Ok<T>(this T value)
        {
            return new Optional<T> { Valid = true, Value = value, ErrorMessage = string.Empty, ErrorCode = 0 };
        }
    }
}

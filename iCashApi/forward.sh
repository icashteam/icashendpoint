﻿# clear
sudo iptables -F -t filter
sudo iptables -X -t filter

sudo iptables -F -t mangle
sudo iptables -X -t mangle

sudo iptables -F -t nat
sudo iptables -X -t nat

sudo iptables -Z


sudo iptables -P INPUT ACCEPT
sudo iptables -P FORWARD ACCEPT
sudo iptables -P OUTPUT ACCEPT

sudo iptables -t nat -P PREROUTING ACCEPT
sudo iptables -t nat -P POSTROUTING ACCEPT

sudo sysctl -w net.ipv4.ip_forward=1




# rules
forward_ip=125.227.145.62
forward_port=8340
output_interface=enp2s0

sudo iptables -t nat -A PREROUTING -p tcp --dport 21 -j DNAT --to-destination ${forward_ip}:21
sudo iptables -t nat -A PREROUTING -p tcp --dport 23:65535 -j DNAT --to-destination ${forward_ip}:23-65535
sudo iptables -t filter -A FORWARD -p tcp -d ${forward_ip} -m state --state NEW,ESTABLISHED,RELATED -j ACCEPT
sudo iptables -t nat -A POSTROUTING -p tcp -d ${forward_ip} -o ${output_interface} -j MASQUERADE

﻿using QP3000ETestTool.Business.Models;
using System;

namespace iCashApi.Models
{
    public class PongResponse : IResponse
    {
        public void OK(object result)
        {
            Console.WriteLine(result);
        }

        public void Error(string errorCode, string ErrorMessage)
        {
            Console.WriteLine(errorCode + " : " + ErrorMessage);
        }
    }
}

﻿using iCashApi.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;


namespace iCashApi.Models
{
    public class RecvHeader
    {
        public List<string> Fields { get; set; }
        public byte[] SomeBody { get; set; }

        public uint ContentLength
        {
            get
            {
                string contentLength = Fields.FirstOrDefault(field => field.ToLower().Contains("content-length:"));
                if (contentLength == null)
                    throw new ContentLengthNotFound();

                return UInt32.Parse(contentLength.ToLower().Replace("content-length:", "").Replace(" ", ""));
            }
        }

        public string Route
        {
            get
            {
                string methodField = Fields.FirstOrDefault( field => field.ToLower().Contains("post") );
                if (methodField == null)
                    throw new NoSuchMethodException();

                string[] descriptions = methodField.Split(' ');
                if(descriptions.Length < 2 || descriptions[1].StartsWith("/") == false )
                    throw new NoRouterException();

                return descriptions[1].ToLower();
            }
        }

        public void Debug()
        {
            Console.WriteLine("------");
            foreach (var field in Fields)
                Console.WriteLine(field);
            Console.WriteLine("------");

            Console.WriteLine("route:" + Route);
        }
    }
}

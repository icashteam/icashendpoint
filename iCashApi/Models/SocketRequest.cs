﻿using QP3000ETestTool.Business.Models;

namespace iCashApi.Models
{
    public class SocketRequest : IRequest
    {
        public IResponse Response { get; set; }
        public string Command { get; set; }
        public object Parameter { get; set; }
    }
}

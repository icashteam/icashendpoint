﻿using QP3000ETestTool.Business.Models;
using System;
using System.Net.Sockets;
using System.Text;
using iCashApi.Extensions;
using Newtonsoft.Json;

namespace iCashApi.Models
{
    public class SocketResponse : IResponse
    {
        private readonly Socket conn;

        public SocketResponse(Socket conn)
        {
            this.conn = conn;
        }

        public void Error(string errorCode, string ErrorMessage)
        {
            try
            {
                byte[] json = errorCode.Error(ErrorMessage);
                conn.SendJson( json );
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.CloseNoThrow();
            }
        }

        public void OK(object result)
        {
            try
            {
                byte[] json = result.OK();
                conn.SendJson( json );
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                conn.CloseNoThrow();
            }
        }
    }

    public class HwResult
    {
        public bool Valid { get; set; }
        public string ResultMessage { get; set; }
        public string ResultCode { get; set; }
        public object Value { get; set; }
    }

    static class Private
    {
        public static byte[] OK(this object result)
        {
            HwResult errorResult = new HwResult
            {
                Valid = true,
                Value = result,
            };
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(errorResult));
        }

        public static byte[] Error(this string errorCode, string ErrorMessage)
        {
            HwResult errorResult = new HwResult
            {
                Valid = false,
                ResultCode = errorCode,
                ResultMessage = ErrorMessage
            };
            return Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(errorResult));
        }
    }
}

﻿using QP3000ETestTool.Business.Models;


namespace iCashApi.Models
{
    public class PingRequest : IRequest
    {
        public IResponse Response { get; set; } = new PongResponse { };
        public string Command { get; set; } = "ping";
        public object Parameter { get; set; }
    }
}

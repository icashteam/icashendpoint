﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using iCashApi.Exceptions;
using iCashApi.Models;
using Newtonsoft.Json;
using QP3000ETestTool.Business.Models;
using QP3000ETestTool.Models.Parameter;

namespace iCashApi.Extensions
{
    public static class SocketExtension
    {
        private const string HttpHeaderNewLine = "\r\n\r\n";
        private const int RequestMaxLength = 10 * 4096;

        public static IRequest RecvRequest(this Socket conn)
        {
            Console.WriteLine("before RecvHttpHeader");
            RecvHeader header = conn.RecvHttpHeader();
            header.Debug();
            header.CheckSafe();

            Console.WriteLine("before RecvHttpBody");
            string body = conn.RecvHttpBody(header);
            Console.WriteLine("body:---" + body + "---");

            return new SocketRequest
            {
                Response = new SocketResponse(conn),
                Command = header.Route.Substring(1),
                Parameter = JsonConvert.DeserializeObject<TradeParameter>(body)
            };
        }


        public static void CloseNoThrow(this Socket conn)
        {
            try
            {
                conn.Shutdown(SocketShutdown.Both);
                conn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public static void SendJson(this Socket conn, byte[] bytes)
        {
            conn.Send(Encoding.ASCII.GetBytes("HTTP/1.0 200 OK\r\n"));
            conn.Send(Encoding.ASCII.GetBytes("Access-Control-Allow-Origin: *\r\n"));
            conn.Send(Encoding.ASCII.GetBytes("Connection: Close\r\n"));
            conn.Send(Encoding.ASCII.GetBytes("Content-Type: application/json ; charset=utf-8\r\n"));
            conn.Send(Encoding.ASCII.GetBytes($"Content-Length: {bytes.Length}\r\n\r\n"));
            conn.Send(bytes);
        }

        private static RecvHeader RecvHttpHeader(this Socket conn)
        {
            string data = conn.ReadUntilHttpNewLine();

            int separtIndex = data.LastIndexOf(HttpHeaderNewLine) + HttpHeaderNewLine.Length;
            return new RecvHeader
            {
                Fields = data.Substring(0, separtIndex).Split(new char[] { '\r', '\n' }).Where(str => !string.IsNullOrWhiteSpace(str)).ToList(),
                SomeBody = Encoding.ASCII.GetBytes(data.Substring(separtIndex))
            };
        }

        private static string RecvHttpBody(this Socket conn, RecvHeader header)
        {
            List<byte> body = header.SomeBody.ToList();
            while (body.Count < header.ContentLength)
            {
                var bytes = new byte[1024];
                int bytesRec = conn.Receive(bytes);
                if (bytesRec == 0)
                    throw new ClientConnectShutdown { };

                body.AddRange(bytes.Take(bytesRec).ToList());
            }

            return Encoding.UTF8.GetString(body.ToArray());
        }

        private static string ReadSomeASCII(this Socket conn)
        {
            var bytes = new byte[1024];
            int bytesRec = conn.Receive(bytes);
            if (bytesRec == 0)
                throw new ClientConnectShutdown { };

            return Encoding.ASCII.GetString(bytes, 0, bytesRec);
        }

        private static string ReadUntilHttpNewLine(this Socket conn)
        {
            StringBuilder data = new StringBuilder();
            do
            {
                data.CheckSafe();
                data.Append( conn.ReadSomeASCII() );

            } while (data.ContainsNewLine() == false);

            return data.ToString();
        }

        private static bool ContainsNewLine(this StringBuilder data)
        {
            return data.ToString().LastIndexOf(HttpHeaderNewLine) != -1;
        }

        private static void CheckSafe(this StringBuilder data)
        {
            if (data.Length > RequestMaxLength)
                throw new HeaderTooLongException();
        }

        private static void CheckSafe(this RecvHeader header)
        {
            if (header.ContentLength > RequestMaxLength)
                throw new BodyTooLongException();
        }
    }
}

﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using QP3000ETestTool.Business;
using QP3000ETestTool.Business.Models;
using QP3000ETestTool.Exceptions;
using QP3000ETestTool.Exceptions.ProjectOnly;
using iCashApi.Models;
using iCashApi.Extensions;
using System.Threading;
using ClassLibrary.Utilities;
using iCashApi.Exceptions;
using QP3000ETestTool.Interface;
using iCashApi.Config;

namespace iCashApi
{
    class Program : EndPointBusiness
    {
        private const string PersistentDirectory = "C:/Users/Public/iCash";
        private readonly string PinFile = $"{PersistentDirectory}/pin.txt";
        private readonly string BlackListFile = $"{PersistentDirectory}/blacklist.dat";

        private Socket listener;
        private object fileMutex;

        static void Main(string[] args)
        {
            EndPointBusiness business = new Program();

            var t1 = new Thread( business.SyncBlackListProcedure );
            var t2 = new Thread( business.CardReaderProcedure );
            var t3 = new Thread( business.PingProcedure );

            t1.Start();
            t2.Start();
            t3.Start();
            t1.Join();
            t2.Join();
            t3.Join();
        }

        public Program()
        {
            listener = new Socket( IPAddress.Any.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            fileMutex   = new object();
        }

        protected override void Precondition()
        {
            if( Directory.Exists(PersistentDirectory) == false )
                throw new PersistenNotExist { };

            listener.Bind(new IPEndPoint( IPAddress.Any, 18000));
            listener.Listen(1);
        }

        protected override string ReadPingCode()
        {
            if ( File.Exists( PinFile ) == false )
                throw new PinCodeNotExistException { };

            string result = File.ReadAllText(PinFile);
            if( string.IsNullOrWhiteSpace(result)  )
                throw new PinCodeNotExistException { };
            else
                return result;
        }

        protected override void UpdatePingCode(string value)
        {
            File.WriteAllText( PinFile, value);
        }

        protected override IRequest GetRequest()
        {
            try
            {
                return DaemonRequest();
            }
            catch (Exception ex)
            {
                return new PingRequest
                {
                    Parameter = ex.Message
                };
            }
        }

        private IRequest DaemonRequest()
        {
            Console.WriteLine("before Accept");
            Socket conn = listener.Accept();
            try
            {
                conn.ReceiveTimeout = 10 * 1000;
                conn.SendTimeout = 120 * 1000;
                return conn.RecvRequest();
            }
            catch(ClientConnectShutdown)
            {
                return new PingRequest { Parameter = "ClientConnectShutdown" };
            }
            catch (Exception ex)
            {
                new SocketResponse(conn).Error( "99999999", "RecvFormatError") ;
                return new PingRequest { Parameter = ex.Message };
            }
        }

        protected override void StoreBlackList(byte[] raw)
        {
            lock(fileMutex)
            {
                File.WriteAllBytes( BlackListFile, raw);
            }
        }

        protected override byte[] LoadBlackList()
        {
            lock (fileMutex)
            {
                return File.ReadAllBytes(BlackListFile);
            }
        }

        protected override void Ping()
        {
            try
            {
               HttpTools.Post( "http://localhost:18000/ping", new byte[] { });
            }
            catch(Exception ex)
            {
                Console.WriteLine( ex.Message );
            }
        }

        protected override IStoreInfo NextTradeConfig()
        {
            return new TradeConfig { };
        }
    }
}

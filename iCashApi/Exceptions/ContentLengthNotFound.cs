﻿using System;


namespace iCashApi.Exceptions
{
    public class ContentLengthNotFound : Exception
    {
        public ContentLengthNotFound()
            : base("ContentLengthNotFound")
        {}
    }
}

﻿using System;

namespace iCashApi.Exceptions
{
    public class BodyTooLongException : Exception
    {
        public BodyTooLongException()
            : base("BodyTooLongException")
        {}
    }
}

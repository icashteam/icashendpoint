﻿using System;


namespace iCashApi.Exceptions
{
    public class NoRouterException : Exception
    {
        public NoRouterException()
            :base("NoRouterException")
        {}
    }
}

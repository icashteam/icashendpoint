﻿using System;


namespace iCashApi.Exceptions
{
    public class NoSuchMethodException : Exception
    {
        public NoSuchMethodException()
            :base("NoSuchMethodException")
        {}
    }
}

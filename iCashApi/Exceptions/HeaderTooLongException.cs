﻿using System;


namespace iCashApi.Exceptions
{
    public class HeaderTooLongException : Exception
    {
        public HeaderTooLongException()
            :base("HeaderTooLongException")
        { }
    }
}

﻿using QP3000ETestTool.Interface;
using System.Text;

namespace iCashApi.Config
{
    public class TradeConfig : IStoreInfo
    {
        //特約機構代號 
        public byte[] MarketCode
        {
            get => Encoding.Default.GetBytes("W01");
        }

        // 店號
        public byte[] StoreNO
        {
            get => Encoding.Default.GetBytes("82802149");
        }

        // POS 機號
        public byte[] PosCode
        {
            get => new byte[] { 0x30, 0x30, 0x31 };
        }

        // 收銀機編號
        public byte[] CashierCode
        {
            get => new byte[] { 0x30, 0x30, 0x30, 0x30 };
        }

        // 交易序號
        public byte[] TradeSn
        {
            get => new byte[] { 0x30, 0x30, 0x30, 0x30, 0x30, 0x31 };
        }
    }
}

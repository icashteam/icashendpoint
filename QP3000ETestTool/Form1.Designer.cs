﻿namespace QP3000ETestTool
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器
        /// 修改這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.RunbgWorker = new System.ComponentModel.BackgroundWorker();
            this.maintabPage = new System.Windows.Forms.TabPage();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.updatebut = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pincodetext = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.terminalbut = new System.Windows.Forms.Button();
            this.setupbut = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.getverbut = new System.Windows.Forms.Button();
            this.querybut = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.chargecancelbut = new System.Windows.Forms.Button();
            this.refundbut = new System.Windows.Forms.Button();
            this.chargebut = new System.Windows.Forms.Button();
            this.transamttext = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.salebut = new System.Windows.Forms.Button();
            this.maintabControl = new System.Windows.Forms.TabControl();
            this.maintabPage.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.maintabControl.SuspendLayout();
            this.SuspendLayout();
            // 
            // maintabPage
            // 
            this.maintabPage.Controls.Add(this.richTextBox1);
            this.maintabPage.Controls.Add(this.groupBox4);
            this.maintabPage.Controls.Add(this.groupBox1);
            this.maintabPage.Controls.Add(this.groupBox2);
            this.maintabPage.Controls.Add(this.groupBox3);
            this.maintabPage.Location = new System.Drawing.Point(4, 29);
            this.maintabPage.Name = "maintabPage";
            this.maintabPage.Padding = new System.Windows.Forms.Padding(3);
            this.maintabPage.Size = new System.Drawing.Size(1002, 639);
            this.maintabPage.TabIndex = 0;
            this.maintabPage.Text = "主功能";
            this.maintabPage.UseVisualStyleBackColor = true;
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(661, 34);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(328, 592);
            this.richTextBox1.TabIndex = 7;
            this.richTextBox1.Text = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.updatebut);
            this.groupBox4.Location = new System.Drawing.Point(16, 464);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(141, 148);
            this.groupBox4.TabIndex = 8;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "版本更新";
            // 
            // updatebut
            // 
            this.updatebut.Location = new System.Drawing.Point(6, 28);
            this.updatebut.Name = "updatebut";
            this.updatebut.Size = new System.Drawing.Size(127, 93);
            this.updatebut.TabIndex = 0;
            this.updatebut.Text = "韌體更新";
            this.updatebut.UseVisualStyleBackColor = true;
            this.updatebut.Click += new System.EventHandler(this.updatebut_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pincodetext);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.terminalbut);
            this.groupBox1.Controls.Add(this.setupbut);
            this.groupBox1.Location = new System.Drawing.Point(16, 19);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(318, 193);
            this.groupBox1.TabIndex = 4;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "管理類";
            // 
            // pincodetext
            // 
            this.pincodetext.Location = new System.Drawing.Point(137, 154);
            this.pincodetext.MaxLength = 4;
            this.pincodetext.Name = "pincodetext";
            this.pincodetext.ReadOnly = true;
            this.pincodetext.Size = new System.Drawing.Size(50, 29);
            this.pincodetext.TabIndex = 3;
            this.pincodetext.Text = "0000";
            this.pincodetext.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 157);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(93, 20);
            this.label1.TabIndex = 2;
            this.label1.Text = "PIN Code : ";
            // 
            // terminalbut
            // 
            this.terminalbut.Location = new System.Drawing.Point(185, 30);
            this.terminalbut.Margin = new System.Windows.Forms.Padding(5);
            this.terminalbut.Name = "terminalbut";
            this.terminalbut.Size = new System.Drawing.Size(125, 98);
            this.terminalbut.TabIndex = 1;
            this.terminalbut.Text = "開機授權";
            this.terminalbut.UseVisualStyleBackColor = true;
            this.terminalbut.Click += new System.EventHandler(this.terminalbut_Click);
            // 
            // setupbut
            // 
            this.setupbut.Location = new System.Drawing.Point(8, 30);
            this.setupbut.Margin = new System.Windows.Forms.Padding(5);
            this.setupbut.Name = "setupbut";
            this.setupbut.Size = new System.Drawing.Size(125, 98);
            this.setupbut.TabIndex = 0;
            this.setupbut.Text = "申裝開通";
            this.setupbut.UseVisualStyleBackColor = true;
            this.setupbut.Click += new System.EventHandler(this.setupbut_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.getverbut);
            this.groupBox2.Controls.Add(this.querybut);
            this.groupBox2.Location = new System.Drawing.Point(345, 19);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(310, 140);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "查詢類";
            // 
            // getverbut
            // 
            this.getverbut.Location = new System.Drawing.Point(185, 30);
            this.getverbut.Name = "getverbut";
            this.getverbut.Size = new System.Drawing.Size(119, 97);
            this.getverbut.TabIndex = 3;
            this.getverbut.Text = "讀取版號";
            this.getverbut.UseVisualStyleBackColor = true;
            this.getverbut.Click += new System.EventHandler(this.getverbut_Click);
            // 
            // querybut
            // 
            this.querybut.Location = new System.Drawing.Point(8, 30);
            this.querybut.Margin = new System.Windows.Forms.Padding(5);
            this.querybut.Name = "querybut";
            this.querybut.Size = new System.Drawing.Size(125, 97);
            this.querybut.TabIndex = 2;
            this.querybut.Text = "查詢";
            this.querybut.UseVisualStyleBackColor = true;
            this.querybut.Click += new System.EventHandler(this.querybut_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.chargecancelbut);
            this.groupBox3.Controls.Add(this.refundbut);
            this.groupBox3.Controls.Add(this.chargebut);
            this.groupBox3.Controls.Add(this.transamttext);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.salebut);
            this.groupBox3.Location = new System.Drawing.Point(16, 218);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(639, 180);
            this.groupBox3.TabIndex = 6;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "交易類";
            // 
            // chargecancelbut
            // 
            this.chargecancelbut.Location = new System.Drawing.Point(500, 66);
            this.chargecancelbut.Name = "chargecancelbut";
            this.chargecancelbut.Size = new System.Drawing.Size(119, 93);
            this.chargecancelbut.TabIndex = 8;
            this.chargecancelbut.Text = "加值取消";
            this.chargecancelbut.UseVisualStyleBackColor = true;
            this.chargecancelbut.Click += new System.EventHandler(this.chargecancelbut_Click);
            // 
            // refundbut
            // 
            this.refundbut.Location = new System.Drawing.Point(337, 66);
            this.refundbut.Name = "refundbut";
            this.refundbut.Size = new System.Drawing.Size(125, 93);
            this.refundbut.TabIndex = 7;
            this.refundbut.Text = "退貨";
            this.refundbut.UseVisualStyleBackColor = true;
            this.refundbut.Click += new System.EventHandler(this.refundbut_Click);
            // 
            // chargebut
            // 
            this.chargebut.Location = new System.Drawing.Point(175, 66);
            this.chargebut.Name = "chargebut";
            this.chargebut.Size = new System.Drawing.Size(125, 93);
            this.chargebut.TabIndex = 6;
            this.chargebut.Text = "加值";
            this.chargebut.UseVisualStyleBackColor = true;
            this.chargebut.Click += new System.EventHandler(this.chargebut_Click);
            // 
            // transamttext
            // 
            this.transamttext.Location = new System.Drawing.Point(110, 29);
            this.transamttext.MaxLength = 5;
            this.transamttext.Name = "transamttext";
            this.transamttext.Size = new System.Drawing.Size(77, 29);
            this.transamttext.TabIndex = 5;
            this.transamttext.Text = "1";
            this.transamttext.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(7, 29);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 4;
            this.label2.Text = "交易金額 : ";
            // 
            // salebut
            // 
            this.salebut.Location = new System.Drawing.Point(8, 66);
            this.salebut.Margin = new System.Windows.Forms.Padding(5);
            this.salebut.Name = "salebut";
            this.salebut.Size = new System.Drawing.Size(125, 93);
            this.salebut.TabIndex = 3;
            this.salebut.Text = "扣款";
            this.salebut.UseVisualStyleBackColor = true;
            this.salebut.Click += new System.EventHandler(this.salebut_Click);
            // 
            // maintabControl
            // 
            this.maintabControl.Controls.Add(this.maintabPage);
            this.maintabControl.Location = new System.Drawing.Point(12, 12);
            this.maintabControl.Name = "maintabControl";
            this.maintabControl.SelectedIndex = 0;
            this.maintabControl.Size = new System.Drawing.Size(1010, 672);
            this.maintabControl.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1034, 770);
            this.Controls.Add(this.maintabControl);
            this.Font = new System.Drawing.Font("微軟正黑體", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.Margin = new System.Windows.Forms.Padding(5);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "QP3000E 測試工具 V1.0.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.maintabPage.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.maintabControl.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker RunbgWorker;
        private System.Windows.Forms.TabPage maintabPage;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button updatebut;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox pincodetext;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button terminalbut;
        private System.Windows.Forms.Button setupbut;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button getverbut;
        private System.Windows.Forms.Button querybut;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button chargecancelbut;
        private System.Windows.Forms.Button refundbut;
        private System.Windows.Forms.Button chargebut;
        private System.Windows.Forms.TextBox transamttext;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button salebut;
        private System.Windows.Forms.TabControl maintabControl;
    }
}


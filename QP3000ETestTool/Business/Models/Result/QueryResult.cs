﻿
using QP3000ETestTool.Exceptions.ProjectOnly;
using System;

namespace QP3000ETestTool.Models
{
    public class QueryResult
    {
        // 卡號
        public string CardNO
        {
            get
            {
                return System.Text.Encoding.Default.GetString(rawCardNO);
            }
        }

        // 餘額
        public int Balance
        {
            get
            {
                try
                {
                    return int.Parse(System.Text.Encoding.Default.GetString(rawBalance));
                }
                catch(Exception)
                {
                    return int.MinValue;
                }
            }
        }

        // 卡別
        public string CardType
        {
            get
            {
                return System.Text.Encoding.Default.GetString(rawCardType);
            }
        }

        // 有效期限
        public string ExpiredDate
        {
            get
            {
                return System.Text.Encoding.Default.GetString(rawExpiredDate);
            }
        }

        // 卡片狀態
        public string CardStatus
        {
            get
            {
                return System.Text.Encoding.Default.GetString(rawCardStatus);
            }
        }

        // 累點註記
        public string IpointFlag
        {
            get
            {
                return System.Text.Encoding.Default.GetString(rawIpointFlag);
            }
        }

        // 最後一筆交易
        public string LastTxlog
        {
            get
            {
                return System.Text.Encoding.Default.GetString(rawlastTxlog);
            }
        }

        public QueryResult(byte[] baResponse)
        {
            Array.Copy(baResponse, 9, rawCardNO, 0, 16);
            Array.Copy(baResponse, 25, rawBalance, 0, 8);
            Array.Copy(baResponse, 33, rawCardType, 0, 2);
            Array.Copy(baResponse, 35, rawExpiredDate, 0, 8);
            Array.Copy(baResponse, 43, rawCardStatus, 0, 2);
            Array.Copy(baResponse, 45, rawIpointFlag, 0, 2);
            Array.Copy(baResponse, 47, rawlastTxlog, 0, 24);
        }

        private byte[] rawCardNO = new byte[16];
        private byte[] rawBalance = new byte[8];
        private byte[] rawCardType = new byte[2];
        private byte[] rawExpiredDate = new byte[8];
        private byte[] rawCardStatus = new byte[2];
        private byte[] rawIpointFlag = new byte[2];
        private byte[] rawlastTxlog = new byte[24];
    }
}

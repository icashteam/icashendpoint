﻿using System;
using System.Text;

namespace QP3000ETestTool.Models
{
    public class SaleResult
    {
        public string BeforeTrade
        {
            get => Encoding.Default.GetString(rawBeforeTrade);
        }

        public string AfterTrade
        {
            get => Encoding.Default.GetString(rawAfterTrade);
        }

        public int SaleAmount
        {
            get; private set;
        }

        public string TradeSN { get; set; }

        public string Cookie { get; set; }

        public string AutoloadAmount { get; private set; }

        public SaleResult(byte[] baResponse, byte[] baAutoloadAmount, int saleAmount)
        {
            Array.Copy(baResponse, 25, rawBeforeTrade, 0, 8);
            Array.Copy(baResponse, 33, rawAfterTrade, 0, 8);
            AutoloadAmount = Encoding.Default.GetString(baAutoloadAmount);
            SaleAmount = saleAmount;
        }
        
        private byte[] rawBeforeTrade = new byte[8];
        private byte[] rawAfterTrade = new byte[8];
    }
}

﻿using System;
using System.Text;

namespace QP3000ETestTool.Models
{
    public class ChargeCancelResult
    {
        public string BeforeTrade
        {
            get => Encoding.Default.GetString(rawBeforeTrade);
        }

        public string AfterTrade
        {
            get => Encoding.Default.GetString(rawAfterTrade);
        }

        public int CancelAmount
        {
            get; private set;
        }

        public string TradeSN { get; set; }
        public string Cookie { get; set; }

        public ChargeCancelResult(byte[] baResponse, int cancelAmount)
        {
            Array.Copy(baResponse, 25, rawBeforeTrade, 0, 8);
            Array.Copy(baResponse, 33, rawAfterTrade, 0, 8);
            CancelAmount = cancelAmount;
        }

        private byte[] rawBeforeTrade = new byte[8];
        private byte[] rawAfterTrade = new byte[8];
    }
}

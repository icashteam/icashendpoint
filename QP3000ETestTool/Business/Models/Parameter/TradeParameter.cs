﻿namespace QP3000ETestTool.Models.Parameter
{
    public class TradeParameter
    {
        public string Name { get; set; }
        public int Amount { get; set; }
        public string Cookie { get; set; }
    }
}

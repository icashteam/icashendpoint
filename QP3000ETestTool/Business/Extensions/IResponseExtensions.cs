﻿using QP3000ETestTool.Business.Models;
using QP3000ETestTool.Exceptions;
using QP3000ETestTool.Interface;
using QP3000ETestTool.Models;

namespace QP3000ETestTool.Business.Extensions
{
    public static class IResponseExtensions
    {
        // 查詢
        public static QueryResult QueryCardInfo(this IResponse response, IStoreInfo storeConfig)
        {
            byte[] baResponse = new byte[1024];

            uint irc = iCashServant.QueryCardInfo( StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, baResponse);
            Helper.ExceptionGuard(irc, response);
            return new QueryResult(baResponse);
        }

        // 扣款
        public static SaleResult TradeSale(this IResponse response, int saleAmount)
        {
            byte[] baResponse = new byte[1024];
            byte[] baAutoloadAmount = new byte[8];

            uint irc = iCashServant.TradeSale(StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, saleAmount, baAutoloadAmount, baResponse);
            Helper.ExceptionGuard(irc, response);
            return new SaleResult(baResponse, baAutoloadAmount, saleAmount);
        }

        // 加值
        public static ChargeResult TradeCharge(this IResponse response, int chargeAmount)
        {
            byte[] baResponse = new byte[1024];

            uint irc = iCashServant.TradeCharge(StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, chargeAmount, baResponse);
            Helper.ExceptionGuard(irc, response);
            return new ChargeResult(baResponse, chargeAmount);
        }

        // 取消加值
        public static ChargeCancelResult TradeChargeCancel(this IResponse response, int cost)
        {
            byte[] baResponse = new byte[1024];

            uint irc = iCashServant.TradeChargeCancel(StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, cost, baResponse);
            Helper.ExceptionGuard(irc, response);
            return new ChargeCancelResult(baResponse, cost);
        }

        // 退貨加值
        public static RefundResult TradeRefund(this IResponse response, int cost)
        {
            byte[] baResponse = new byte[1024];

            uint irc = iCashServant.TradeRefund(StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, cost, baResponse);
            Helper.ExceptionGuard(irc, response);
            return new RefundResult(baResponse, cost);
        }
    }
}

﻿using QP3000ETestTool.Business.Models;
using QP3000ETestTool.Exceptions.ProjectOnly;
using QP3000ETestTool.Interface;
using QP3000ETestTool.Models;
using QP3000ETestTool.Models.Parameter;
using System.Collections.Generic;

namespace QP3000ETestTool.Business.Extensions
{
    public static class IRequestExtension
    {
        public static void Query(this IRequest request, List<string> blackList, IStoreInfo storeConfig)
        {
            QueryResult query = request.Response.QueryCardInfo(storeConfig);
            request.GuardBlackList( blackList, query.CardNO );

            request.Response.OK(query);
        }

        public static void Sale(this IRequest request, List<string> blackList, IStoreInfo storeConfig)
        {
            QueryResult query = request.Response.QueryCardInfo(storeConfig);
            request.GuardBlackList(blackList, query.CardNO);

            TradeParameter parameter = request.Expert<TradeParameter>();
            if (query.Balance < parameter.Amount)
                throw new CheckBalanceNotEnought { Response = request.Response };
            else
                request.Response.OK(request.Response.TradeSale(parameter.Amount));
        }

        public static void Charge(this IRequest request, List<string> blackList)
        {
            TradeParameter parameter = request.Expert<TradeParameter>();

            request.Response.OK( request.Response.TradeCharge(parameter.Amount) );
        }

        public static void Charge_Cancel(this IRequest request, List<string> blackList)
        {
            TradeParameter parameter = request.Expert<TradeParameter>();

            request.Response.OK( request.Response.TradeChargeCancel(parameter.Amount ));
        }

        public static void Refund(this IRequest request, List<string> blackList)
        {
            TradeParameter parameter = request.Expert<TradeParameter>();

            request.Response.OK( request.Response.TradeRefund(parameter.Amount) );
        }

        public static void Pong(this IRequest request)
        {
            request.Response.OK(request.Parameter);
        }

        public static T Expert<T>(this IRequest request)
        {
            if (request.Parameter != null && request.Parameter is T)
                return (T)request.Parameter;

            throw new ApiParameterError { Response = request.Response };
        }

        private static void GuardBlackList(this IRequest request, List<string> blackList, string CardNo)
        {
            if (blackList.BinarySearch(CardNo) >= 0)
                throw new CheckBlackListFoundException { Response = request.Response };
        }
    }
}

﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using BlackLIstParser.Business.Models;
using QP3000ETestTool.Business.Extensions;
using QP3000ETestTool.Business.Models;
using QP3000ETestTool.Exceptions;
using QP3000ETestTool.Exceptions.ProjectOnly;
using System.Net;
using ClassLibrary.Extensions;
using System.IO;
using QP3000ETestTool.Interface;

namespace QP3000ETestTool.Business
{
    public abstract partial class EndPointBusiness
    {
        private const int PingInterval = 60 * 60 * 10; // 每10hr SignOn 一次
        private const int SyncBlackListInterval = 60 * 60 * 24; // 每24hr下載一次黑名單 

        private DateTime LastSignOnTime = DateTime.MinValue;

        public void CardReaderProcedure()
        {
            while(true)
            {
                try
                {
                    Precondition();
                    SetupReaderLayer();
                }
                catch (EndPointDeprecate ex)
                {
                    ex.Echo("端末設置錯誤 ( 無該端末於特店設置記錄  )");
                    return;
                }
                catch (SetupReaderEndPointNotFound ex)
                {
                    ex.Echo("查無端末  ( 中心端無該端末相關資訊  )");
                    return;
                }
                catch (TermAuthEndPointNotFound ex)
                {
                    ex.Echo("端末不符  ( 中心端無該端末相關資訊  )");
                    return;
                }
                catch (CommonEndPointNotFound ex)
                {
                    ex.Echo("端末設置錯誤 ( 無該端末於特店設置記錄  ) ");
                    return;
                }
                catch (EndPointNotMatch ex)
                {
                    ex.Echo("端末配對錯誤  (POS 硬碟、機號和店號配對不正確 )");
                    return;
                }
                catch (SamNotFoundException ex)
                {
                    ex.Echo("查無 SAM ( 中心端無該SAM 資訊  )");
                    return;
                }
                catch (StoreNotFoundException1 ex)
                {
                    ex.Echo("中心端無該特店資訊");
                    return;
                }
                catch (StoreNotFoundException2 ex)
                {
                    ex.Echo("中心端無該特店資訊");
                    return;
                }
                catch (StoreNotFoundException3 ex)
                {
                    ex.Echo("中心端無該特店資訊");
                    return;
                }
                catch (AccessDirectoryError ex)
                {
                    ex.Echo("檢查資料夾失敗");
                    return;
                }
                catch (iniException ex)
                {
                    ex.Echo("讀取參數檔失敗");
                    return;
                }
                catch (ReadKeyFails ex)
                {
                    ex.Echo("讀取錄碼 Key 失敗");
                    return;
                }
                catch (ResetReaderError ex)
                {
                    ex.Echo("Reset Reader 失敗");
                    return;
                }
                catch (SamResetError ex)
                {
                    ex.Echo("交易安全模組(TPLSAM/SAM AV2) Reset 失敗");
                    return;
                }
                catch (USBAccessException ex)
                {
                    ex.Echo("USB 通訊異常");
                    return;
                }
                catch (WriteKeyFails ex)
                {
                    ex.Echo("寫入錄碼 Key 失敗");
                    return;
                }
                catch (PinCodeGetError ex)
                {
                    ex.Echo("授權碼取得失敗 ( 中心端產生授權碼失敗  )");
                    return;
                }
                catch (SetupReaderNetworkError ex)
                {
                    ex.Echo("後台通訊錯誤");
                    Thread.Sleep(60);
                    continue;
                }
                catch (SocketConnectError ex)
                {
                    ex.Echo("Socket 連線異常");
                    Thread.Sleep(10);
                    continue;
                }
                catch (SocketLengthError ex)
                {
                    ex.Echo("Socket 接收資料長度不足");
                    Thread.Sleep(10);
                    continue;
                }
                catch (ReaderNotEnable ex)
                {
                    ex.Echo("錢包未授權啟用");
                    return;
                }
                catch (AuthCodeError ex)
                {
                    ex.Echo("授權代碼認證錯誤");
                    return;
                }
                catch (iCashError ex)
                {
                    ex.Echo("未分類錯誤 : " + ex.ErrorCode);
                    return;
                }
                catch (PersistenNotExist)
                {
                    Console.WriteLine("無法取得暫存工作區");
                    return;
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    return;
                }
            }
        }

        private void SetupReaderLayer()
        {
            while (true)
            {
                try
                {
                    string pinCode = ReadPingCode();
                    SignOnLayer( pinCode );
                }
                catch (PinCodeNotExistException)
                {
                    Console.WriteLine("PinCode 不存在");
                    UpdatePingCode( SetupReader() );
                }
                catch (PinCodeNotMatch ex)
                {
                    ex.Echo("檢核配對錯誤(端末配對與端末開通不符合)");
                    UpdatePingCode( SetupReader() );
                }
                catch (PinCodeError ex)
                {
                    ex.Echo("PIN 碼認證失敗");
                    UpdatePingCode( SetupReader() );
                }
                catch (SignOnAuthError ex)
                {
                    ex.Echo("授權認證失敗");
                    UpdatePingCode( SetupReader() );
                }
                catch (CommonAuthError ex)
                {
                    ex.Echo("認證錯誤");
                    UpdatePingCode( SetupReader() );
                }
                catch(SignOnNetworkError ex)
                {
                    ex.Echo("後台通訊錯誤");
                    Thread.Sleep(10);
                    continue;
                }
                catch (SocketConnectError ex)
                {
                    ex.Echo("Socket 連線異常");
                    Thread.Sleep(10);
                    continue;
                }
                catch (SocketLengthError ex)
                {
                    ex.Echo("Socket 接收資料長度不足");
                    Thread.Sleep(10);
                    continue;
                }
            }
        }

        private void SignOnLayer(string pinCode)
        {
            while (true)
            {
                try
                {
                    ReaderWorkingLayer();
                }
                catch (AuthExpired ex)
                {
                    ex.Echo("認證過期");
                    LastSignOnTime = DateTime.MinValue;
                    TermAuth(pinCode);
                    LastSignOnTime = DateTime.Now;
                }
                catch (SignOnExpiredException)
                {
                    Console.WriteLine("定期認證機制啟動");
                    LastSignOnTime = DateTime.MinValue;
                    TermAuth(pinCode);
                    LastSignOnTime = DateTime.Now;
                }
            }
        }

        private void ReaderWorkingLayer()
        {
            while (true)
            {
                try
                {
                    CheckSignOnExpired();
                    CheckRequest();
                }
                catch (ApiQueryError ex)
                {
                    ex.Echo("卡機 api 查詢失敗");
                }
                catch (BlackListException ex)
                {
                    ex.Echo("此卡為黑名單");
                }
                catch (BonusNotEnought ex)
                {
                    ex.Echo("交易金額錯誤 (負值)");
                }
                catch (CoBranderCardNotAuth ex)
                {
                    ex.Echo("無法授權 (聯名卡無法授權 )");
                }
                catch (CTagError ex)
                {
                    ex.Echo("Ctag 失敗");
                }
                catch (MessageFormatError ex)
                {
                    ex.Echo("電文格式錯誤");
                }
                catch (CommonNetworkError ex)
                {
                    ex.Echo("前台傳輸錯誤 (Timeout over 20 seconds)");
                }
                catch (CommonNetworkError1 ex)
                {
                    ex.Echo("後台通訊錯誤");
                }
                catch (CommonNetworkError2 ex)
                {
                    ex.Echo("後台通訊錯誤");
                }
                catch (NotRegularCardException ex)
                {
                    ex.Echo("非有效卡  (POS 硬碟、機號和店號配對不正確 )");
                }
                catch (OnlineTxlogFormatError ex)
                {
                    ex.Echo("Txlog 資料錯誤");
                }
                catch (OnlineTxlogRepeat ex)
                {
                    ex.Echo("重複資料 (該筆 Online Txlog 為重複資料 )");
                }
                catch (QuotaNotEnought ex)
                {
                    ex.Echo("交易金額錯誤 (負值)");
                }
                catch (iCashCardBlackList ex)
                {
                    ex.Echo("此卡為黑名單卡片");
                }
                catch (iCashCardBonusLimitException ex)
                {
                    ex.Echo("icasH 卡片累計－款總額超過上限");
                }
                catch (iCashCardBonusNotEnough ex)
                {
                    ex.Echo("icasH 卡片餘額不足");
                }
                catch (iCashCardChargeAmountOverfloat ex)
                {
                    ex.Echo(" icasH 卡片累計加值總額超過上限（99999999）");
                }
                catch (iCashCardChargeSerialOverfloat ex)
                {
                    ex.Echo("icasH 卡片加值序號超過上限（99999999） ");
                }
                catch (iCashCardRWFails ex)
                {
                    ex.Echo(" icasH 卡片讀寫失敗");
                }
                catch (iCashCardSerialLimitException ex)
                {
                    ex.Echo("icasH icasH 卡片累計－款序號超過上限");
                }
                catch (iCashCardTradeAmoutLimit ex)
                {
                    ex.Echo("二代卡交易金額超過限制(小於 0 或大於 1 萬) ");
                }
                catch (iCashCardTradeSerialOverfloat ex)
                {
                    ex.Echo("icasH 卡片累計交易序號超過上限（99999999）");
                }
                catch (iCashCardValidateError ex)
                {
                    ex.Echo(" icasH 卡號檢核錯誤 ");
                }
                catch (iCashCenterAuthError ex)
                {
                    ex.Echo("icasH 聯名卡中心端授權失敗");
                }
                catch (iCashCenterResponseCodeError ex)
                {
                    ex.Echo("icasH 中心端回應代碼錯誤");
                }
                catch (iCashResponseLengthError ex)
                {
                    ex.Echo("icasH 卡片回應資料長度錯誤");
                }
                catch (iCashCardSerialNotMatch ex)
                {
                    ex.Echo(" icasH API 加值/購貨取消/前筆交易查詢卡號不符 ");
                }
                catch (MutilCardException ex)
                {
                    ex.Echo("多卡");
                }
                catch (NotiCashCardException ex)
                {
                    ex.Echo("非 icash 卡(尋卡時回應)");
                }
                catch (OnlineMessageFormatError ex)
                {
                    ex.Echo("Online 交易時，檢查電文格式錯誤");
                }
                catch (OnlineMessageMACError ex)
                {
                    ex.Echo("Online 交易時，檢查電文 MAC 不正確");
                }
                catch (ParameterError ex)
                {
                    ex.Echo("參數錯誤");
                }
                catch (SocketConnectError ex)
                {
                    ex.Echo("Socket 連線異常");
                    Thread.Sleep(10);
                }
                catch (SocketLengthError ex)
                {
                    ex.Echo("Socket 接收資料長度不足");
                    Thread.Sleep(10);
                }
                catch (StoreBonusNotEnought ex)
                {
                    ex.Echo("門市可撥款餘額不足");
                }
                catch (TradeAmoutValidateError ex)
                {
                    ex.Echo("交易金額檢查錯誤");
                }
                catch (PSAMError ex)
                {
                    ex.Echo("PSAM 無法收取卡片之－款轉入款項，已將 icasH 作＋值沖正");
                }
                catch (StoreFunctionNotAvailable ex)
                {
                    ex.Echo("特店不開放加值");
                }
                catch (CardNotFound ex)
                {
                    ex.Echo("找不到卡片");
                }
                catch (CardNotIdentityException ex)
                {
                    ex.Echo("找不到卡片");
                }
                catch (CoBranderCardChargeDisable ex)
                {
                    ex.Echo("icasH 聯名卡啟用代行授權時，卡片並未開啟離線加值設定");
                }
                catch (iCashCardExpired ex)
                {
                    ex.Echo("icasH 卡片超過使用有效期限 ");
                }
                catch (iCashCardLocked ex)
                {
                    ex.Echo("icasH icasH 卡片已鎖");
                }
                catch (iCashCardRefundForbiden ex)
                {
                    ex.Echo("退費時檢查 icasH 記載之前筆交易，並非儲值交易，無法退費");
                }
                catch (iCashCardRefundNotMatch ex)
                {
                    ex.Echo("退費金額與 icasH 記載之前筆儲值交易金額不符");
                }
                catch (iCashCardRemainderNotEnought ex)
                {
                    ex.Echo(" icasH 卡片超過卡片可用餘額上限");
                }
                catch(ApiParameterError ex)
                {
                    ex.Response.Error( "95279000", "前置條件: 溝通格式錯誤");
                }
                catch(CheckBalanceNotEnought ex)
                {
                    ex.Response.Error( "95279001", "前置條件: 卡片餘額不足");
                }
                catch(CheckBlackListFoundException ex)
                {
                    ex.Response.Error("95279002", "前置條件: 本卡為黑名單");
                }
            }
        }

        private void CheckRequest()
        {
            Console.WriteLine("\n\n\n\n----------------Daemon Command");

            var request = GetRequest();
            switch ( request.Command )
            {
                case "1":
                case "query":
                    request.Query( GetBlackList(), NextTradeConfig() );
                    break;

                case "2":
                case "sale":
                    request.Sale( GetBlackList(), NextTradeConfig() );
                    break;

                case "3":
                case "charge":
                    request.Charge( GetBlackList() );
                    break;

                case "4":
                case "charge_cancel":
                    request.Charge_Cancel( GetBlackList() );
                    break;

                case "5":
                case "refund":
                    request.Refund( GetBlackList() );
                    break;

                case "ping":
                    request.Pong();
                    break;

                default:
                    request.Response.Error( "95270000", "Unknown Command");
                    break;
            }
        }

        private void CheckSignOnExpired()
        {
            Console.WriteLine("測試SignOn是否過期?");
            if ( DateTime.Now.Subtract(LastSignOnTime).TotalHours >= 8 )
                throw new SignOnExpiredException { };
            Console.WriteLine("無需SignOn");
        }

        // 開通
        private static string SetupReader()
        {
            byte[] baResponse = new byte[1024];
            byte[] SysPIN = new byte[4];

            Helper.ExceptionGuard(

                iCashServant.SetupReader(StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, SysPIN, baResponse)
            );
            return Encoding.Default.GetString(SysPIN);
        }

        // 驗證
        private static void TermAuth(string pinCode)
        {
            byte[] SysPIN = Encoding.Default.GetBytes(pinCode);
            byte[] baResponse = new byte[1024];

            uint irc = iCashServant.TermAuth(
                StoreInfo.MarketCode,
                StoreInfo.StoreNO,
                StoreInfo.PosCode,
                StoreInfo.TradeSn,
                StoreInfo.CashierCode,
                SysPIN,
                baResponse);

            Helper.ExceptionGuard(irc);
        }

        protected abstract void Precondition();
        protected abstract string ReadPingCode();
        protected abstract void UpdatePingCode(string value);
        protected abstract void StoreBlackList(byte[] raw);
        protected abstract byte[] LoadBlackList();
        protected abstract IRequest GetRequest();
        protected abstract IStoreInfo NextTradeConfig();
        protected abstract void Ping();
    }

    public abstract partial class EndPointBusiness
    {
        private object blackListMutex = new object();
        private List<string> blackList = null;

        public void SyncBlackListProcedure()
        {
            while (true)
            {
                try
                {
                    Console.WriteLine("SyncBlackListProcedure");
                    byte[] rawData = SyncBlackList();
                    UpdateBlackList( new BlackListFormat(rawData) );
                    StoreBlackList( rawData );

                    Thread.Sleep(1000 * SyncBlackListInterval);
                }
                catch(Exception ex)
                {
                    Console.WriteLine( ex.Message );
                }
            }
        }

        private void UpdateBlackList(BlackListFormat blackListFormat)
        {
            lock(blackListMutex)
            {
                blackList = blackListFormat.CardBundle.Cards.Select(card => card.CardNo).ToList();
                blackList.Sort();
            }
        }

        private List<string> GetBlackList()
        {
            try
            {
                lock (blackListMutex)
                {
                    if (blackList == null)
                    {
                        BlackListFormat blackListFormat = new BlackListFormat(LoadBlackList());
                        blackList = blackListFormat.CardBundle.Cards.Select(card => card.CardNo).ToList();
                        blackList.Sort();
                    }
                    return blackList;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                return new List<string> { };
            }
        }

        private byte[] SyncBlackList()
        {
            FtpWebRequest request = FtpWebRequest.Create("ftp://192.168.0.102/out/blacklist.dat") as FtpWebRequest;
            request.KeepAlive = true;
            request.Credentials = new NetworkCredential( "ftpw01", "w011234");
            request.UseBinary = true;
            request.UsePassive = true;
            request.Timeout = 60 * 1000;
            request.Method = WebRequestMethods.Ftp.DownloadFile;

            using (Stream stream = request.GetResponse().GetResponseStream())
            {
                return stream.ReadToEnd();
            }
        }
    }

    public abstract partial class EndPointBusiness
    {
        public void PingProcedure()
        {
            while (true)
            {
                Thread.Sleep(1000 * PingInterval);
                Console.WriteLine("PingProcedure");
                Ping();
            }
        }
    }
}

﻿
namespace QP3000ETestTool.Business.Models
{
    public interface IRequest
    {
        IResponse Response { get;  }
        string Command { get;  }
        object Parameter { get;  }
    }
}

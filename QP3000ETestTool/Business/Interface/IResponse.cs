﻿
namespace QP3000ETestTool.Business.Models
{
    public interface IResponse
    {
        void OK(object result);
        void Error(string errorCode, string ErrorMessage);
    }
}

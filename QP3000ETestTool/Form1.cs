﻿using System;
using System.Windows.Forms;
using QP3000ETestTool.Exceptions;
using QP3000ETestTool.TestCase;

namespace QP3000ETestTool
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void setupbut_Click(object sender, EventArgs e)
        {
            try
            {
                pincodetext.Text = SetupReader.Test();
            }
            catch (Exception ex)
            {
                richTextBox1.Text = ex.Message;
            }
        }

        private void terminalbut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = TermAuth.Test(pincodetext.Text);
        }
               
        private void querybut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = QueryCardInfo.Test();
        }

        private void salebut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = TradeSale.Test( Convert.ToInt16(transamttext.Text) );
        }

        private void chargebut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = TradeCharge.Test( Convert.ToInt16(transamttext.Text) );
        }

        private void getverbut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = GetApVersion.Test() ?? "Fail";
        }

        private void refundbut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = TradeRefund.Test( Convert.ToInt16(transamttext.Text) );
        }

        private void chargecancelbut_Click(object sender, EventArgs e)
        {
            richTextBox1.Text = TradeChargeCancel.Test( Convert.ToInt16(transamttext.Text) );
        }

        private void updatebut_Click(object sender, EventArgs e)
        {
            //richTextBox1.Text = UpdateFW.Test();
            richTextBox1.Text = (0x0000011A).ToString("X8");
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}

﻿

namespace QP3000ETestTool.Interface
{
    public interface IStoreInfo
    {
        //特約機構代號 
        byte[] MarketCode { get; }
        // 店號
        byte[] StoreNO { get; }
        // POS 機號
        byte[] PosCode { get; }
        // 收銀機編號
        byte[] CashierCode { get; }
        // 交易序號
        byte[] TradeSn { get; }
    }
}

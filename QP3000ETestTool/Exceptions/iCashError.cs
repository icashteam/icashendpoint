﻿using QP3000ETestTool.Business.Models;
using System;

namespace QP3000ETestTool.Exceptions
{
    public class iCashError : Exception
    {
        public string ErrorCode { get; private set; }
        public IResponse Response { private get; set; } = new DefaultResponse { };

        public iCashError(string irc)
        {
            ErrorCode = irc;
        }

        public void Echo(string ErrorMessage)
        {
            Response.Error( ErrorCode, ErrorMessage);
        }
    }

    class DefaultResponse : IResponse
    {
        public void Error(string errorCode, string ErrorMessage)
        {
            Console.WriteLine( errorCode + ": " + ErrorMessage);
        }

        public void OK(object result)
        {
            Console.WriteLine(result);
        }
    }
}

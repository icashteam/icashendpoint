﻿using System;
using System.Linq;
using System.Collections.Generic;
using QP3000ETestTool.Business.Models;

namespace QP3000ETestTool.Exceptions
{
    public static class Helper
    {
        public static void ExceptionGuard(uint irc)
        {
            if (irc == 0) return;

            throw Find(irc);
        }

        public static void ExceptionGuard( uint irc, IResponse response)
        {
            if (irc == 0) return;

            iCashError error = Find(irc);

            error.Response = response;
            throw error;
        }


        private static iCashError Find(uint irc)
        {
            string errorCode = irc.ToString("X8");

            iCashError result = SupportException.FirstOrDefault(err => err.ErrorCode == errorCode);
            return result == null ? 
                new iCashError(errorCode) : (iCashError)Activator.CreateInstance(result.GetType());
        }

        
        private static List<iCashError> SupportException
        {
            get => new List<iCashError>
            {
                new ApiQueryError{ },
                new CommonAuthError{ },
                new SignOnAuthError{ },
                new AuthExpired{ },
                new BlackListException{ },
                new BonusNotEnought{ },
                new CoBranderCardNotAuth{ },
                new CTagError{ },
                new EndPointDeprecate{ },
                new SetupReaderEndPointNotFound{ },
                new TermAuthEndPointNotFound{ },
                new CommonEndPointNotFound{ },
                new EndPointNotMatch{ },
                new MessageFormatError{ },
                new CommonNetworkError{ },
                new SignOnNetworkError{ },
                new SetupReaderNetworkError{ },
                new CommonNetworkError1{ },
                new CommonNetworkError2{ },
                new NotRegularCardException{ },
                new OnlineTxlogFormatError{ },
                new OnlineTxlogRepeat{ },
                new PinCodeGetError{ },
                new PinCodeNotMatch{ },
                new QuotaNotEnought{ },
                new SamNotFoundException{ },
                new StoreFunctionNotAvailable{ },
                new StoreNotFoundException1{ },
                new StoreNotFoundException2{ },
                new StoreNotFoundException3{ },
                new AccessDirectoryError{ },
                new AuthCodeError{ },
                new CardNotFound{ },
                new CardNotIdentityException{ },
                new CoBranderCardChargeDisable{ },
                new iCashCardBlackList{ },
                new iCashCardBonusLimitException{ },
                new iCashCardBonusNotEnough{ },
                new iCashCardChargeAmountOverfloat{ },
                new iCashCardChargeSerialOverfloat{ },
                new iCashCardExpired{ },
                new iCashCardLocked{ },
                new iCashCardRefundForbiden{ },
                new iCashCardRefundNotMatch{ },
                new iCashCardRemainderNotEnought{ },
                new iCashCardRWFails{ },
                new iCashCardSerialLimitException{ },
                new iCashCardTradeAmoutLimit{ },
                new iCashCardTradeSerialOverfloat{ },
                new iCashCardValidateError{ },
                new iCashCenterAuthError{ },
                new iCashCenterResponseCodeError{ },
                new iCashResponseLengthError{ },
                new iCashCardSerialNotMatch{ },
                new iniException{ },
                new MutilCardException{ },
                new NotiCashCardException{ },
                new OnlineMessageFormatError{ },
                new OnlineMessageMACError{ },
                new ParameterError{ },
                new PinCodeError{ },
                new PSAMError{ },
                new ReaderNotEnable{ },
                new ReadKeyFails{ },
                new ResetReaderError{ },
                new SamResetError{ },
                new SocketConnectError{ },
                new SocketLengthError{ },
                new StoreBonusNotEnought{ },
                new TradeAmoutValidateError{ },
                new USBAccessException{ },
                new WriteKeyFails{ }
            };
        }
    }
}

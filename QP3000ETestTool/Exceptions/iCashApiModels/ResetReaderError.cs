﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  Reset Reader 失敗  => ??? 人工處理
    /// </summary>
    public class ResetReaderError : iCashError
    {
        public ResetReaderError()
            :base("0000008D")
        {}
    }
}

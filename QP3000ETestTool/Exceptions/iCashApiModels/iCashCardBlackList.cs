﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  此卡為黑名單卡片 => Next
    /// </summary>
    public class iCashCardBlackList : iCashError
    {
        public iCashCardBlackList()
            :base("00000502")
        {}
    }
}

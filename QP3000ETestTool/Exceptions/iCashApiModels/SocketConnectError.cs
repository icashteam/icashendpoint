﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  Socket 連線異常 => Next
    /// </summary>
    public class SocketConnectError : iCashError
    {
        public SocketConnectError()
            :base("00000005")
        {}
    }
}

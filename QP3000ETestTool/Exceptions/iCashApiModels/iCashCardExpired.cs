﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片超過使用有效期限  => Next
    /// </summary>
    public class iCashCardExpired : iCashError
    {
        public iCashCardExpired()
            :base("00000505")
        {}
    }
}

﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  交易安全模組(TPLSAM/SAM AV2) Reset 失敗 => 人工處理
    /// </summary>
    public class SamResetError : iCashError
    {
        public SamResetError()
            :base("00000104")
        {}
    }
}

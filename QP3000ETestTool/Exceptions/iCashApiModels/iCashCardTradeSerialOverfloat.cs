﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// icasH 卡片累計交易序號超過上限（99999999） => Next
    /// </summary>
    public class iCashCardTradeSerialOverfloat : iCashError
    {
        public iCashCardTradeSerialOverfloat()
            :base("00000308")
        {}
    }
}

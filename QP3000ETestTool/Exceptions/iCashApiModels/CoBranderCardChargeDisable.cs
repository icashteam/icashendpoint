﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 聯名卡啟用代行授權時，卡片並未開啟離線加值設定 => Next
    /// </summary>
    public class CoBranderCardChargeDisable : iCashError
    {
        public CoBranderCardChargeDisable()
            :base("00000523")
        {}
    }
}

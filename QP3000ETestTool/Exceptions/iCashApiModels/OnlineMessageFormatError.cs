﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// Online 交易時，檢查電文格式錯誤  => Next
    /// </summary>
    public class OnlineMessageFormatError : iCashError
    {
        public OnlineMessageFormatError()
            :base("00000120")
        {}
    }
}

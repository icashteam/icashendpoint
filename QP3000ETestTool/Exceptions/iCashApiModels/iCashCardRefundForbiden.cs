﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  退費時檢查 icasH 記載之前筆交易，並非儲值交易，無法退費  => Next
    /// </summary>
    public class iCashCardRefundForbiden : iCashError
    {
        public iCashCardRefundForbiden()
            :base("0000010A")
        {}
    }
}

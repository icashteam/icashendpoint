﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  錢包未授權啟用  => 人工處理
    /// </summary>
    public class ReaderNotEnable : iCashError
    {
        public ReaderNotEnable()
            :base("00000090")
        {}
    }
}

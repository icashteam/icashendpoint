﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  檢查資料夾失敗 => 人工處理
    /// </summary>
    public class AccessDirectoryError : iCashError
    {
        public AccessDirectoryError()
            :base("00000003")
        {}
    }
}

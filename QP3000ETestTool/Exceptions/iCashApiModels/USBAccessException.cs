﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  USB 通訊異常 => 人工處理
    /// </summary>
    public class USBAccessException : iCashError
    {
        public USBAccessException()
            :base("00000004")
        {}
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  Socket 接收資料長度不足 => Next
    /// </summary>
    public class SocketLengthError : iCashError
    {
        public SocketLengthError()
            :base("00000006")
        {}
    }
}

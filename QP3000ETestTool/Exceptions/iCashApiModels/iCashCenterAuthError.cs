﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 聯名卡中心端授權失敗  => Next
    /// </summary>
    public class iCashCenterAuthError : iCashError
    {
        public iCashCenterAuthError()
            :base("00000522")
        {}
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// icasH 卡片累計加值總額超過上限（99999999） => Next
    /// </summary>
    public class iCashCardChargeAmountOverfloat : iCashError
    {
        public iCashCardChargeAmountOverfloat()
            :base("00000307")
        {}
    }
}

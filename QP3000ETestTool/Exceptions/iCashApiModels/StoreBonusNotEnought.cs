﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  門市可撥款餘額不足  => Next
    /// </summary>
    public class StoreBonusNotEnought : iCashError
    {
        public StoreBonusNotEnought()
            :base("00000115")
        {}
    }
}

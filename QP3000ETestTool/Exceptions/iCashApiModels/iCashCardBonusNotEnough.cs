﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片餘額不足 => Next
    /// </summary>
    public class iCashCardBonusNotEnough : iCashError
    {
        public iCashCardBonusNotEnough()
            :base("00000106")
        {}
    }
}

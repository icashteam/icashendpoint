﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  交易金額檢查錯誤  => Next
    /// </summary>
    public class TradeAmoutValidateError : iCashError
    {
        public TradeAmoutValidateError()
            :base("00000118")
        {}
    }
}

﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH icasH 卡片已鎖  => Next
    /// </summary>
    public class iCashCardLocked : iCashError
    {
        public iCashCardLocked()
            :base("00000503")
        {}
    }
}

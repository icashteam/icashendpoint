﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片加值序號超過上限（99999999） 
    /// </summary>
    public class iCashCardChargeSerialOverfloat : iCashError
    {
        public iCashCardChargeSerialOverfloat()
            :base("00000306")
        {}
    }
}

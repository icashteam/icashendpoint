﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  PSAM 無法收取卡片之－款轉入款項，已將 icasH 作＋值沖正 => Next
    /// </summary>
    public class PSAMError : iCashError
    {
        public PSAMError()
            :base("00000109")
        {}
    }
}

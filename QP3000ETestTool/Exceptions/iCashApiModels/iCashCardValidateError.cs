﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡號檢核錯誤 => Next
    /// </summary>
    public class iCashCardValidateError : iCashError
    {
        public iCashCardValidateError()
            :base("00000116")
        {}
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片累計－款序號超過上限
    /// </summary>
    public class iCashCardSerialLimitException : iCashError
    {
        public iCashCardSerialLimitException()
            :base("00000107")
        { }
    }
}

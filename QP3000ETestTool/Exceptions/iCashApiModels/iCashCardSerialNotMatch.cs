﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH API 加值/購貨取消/前筆交易查詢卡號不符 => Next
    /// </summary>
    public class iCashCardSerialNotMatch : iCashError
    {
        public iCashCardSerialNotMatch()
            :base("00000346")
        {}
    }
}

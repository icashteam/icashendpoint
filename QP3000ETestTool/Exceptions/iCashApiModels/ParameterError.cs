﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  參數錯誤  => Next
    /// </summary>
    public class ParameterError : iCashError
    {
        public ParameterError()
            :base("00000001")
        {}
    }
}

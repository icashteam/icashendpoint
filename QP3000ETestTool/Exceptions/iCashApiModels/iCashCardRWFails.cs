﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片讀寫失敗 => Next
    /// </summary>
    public class iCashCardRWFails : iCashError
    {
        public iCashCardRWFails()
            :base("00000508")
        {}
    }
}

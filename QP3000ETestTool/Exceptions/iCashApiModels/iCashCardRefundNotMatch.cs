﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  退費金額與 icasH 記載之前筆儲值交易金額不符 => Next
    /// </summary>
    public class iCashCardRefundNotMatch : iCashError
    {
        public iCashCardRefundNotMatch()
            :base("0000010B")
        {

        }
    }
}

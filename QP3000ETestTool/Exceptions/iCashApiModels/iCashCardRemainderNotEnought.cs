﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片超過卡片可用餘額上限 => Next
    /// </summary>
    public class iCashCardRemainderNotEnought : iCashError
    {
        public iCashCardRemainderNotEnought()
            :base("0000010F")
        {}
    }
}

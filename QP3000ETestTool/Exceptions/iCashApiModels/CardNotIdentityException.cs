﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  找不到卡片 => Next
    /// </summary>
    public class CardNotIdentityException : iCashError
    {
        public CardNotIdentityException()
            :base("0000008A")
        {}
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// 寫入錄碼 Key 失敗 => ??? 先人工處理
    /// </summary>
    public class WriteKeyFails : iCashError
    {
        public WriteKeyFails()
            :base("00000112")
        {}        
    }
}

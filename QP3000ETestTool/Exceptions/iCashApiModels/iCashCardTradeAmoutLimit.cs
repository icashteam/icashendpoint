﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  二代卡交易金額超過限制(小於 0 或大於 1 萬)  => Next
    /// </summary>
    public class iCashCardTradeAmoutLimit : iCashError
    {
        public iCashCardTradeAmoutLimit()
            :base("00000110")
        {}
    }
}

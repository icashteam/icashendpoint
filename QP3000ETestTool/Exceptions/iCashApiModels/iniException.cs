﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  讀取參數檔失敗 => 人工介入
    /// </summary>
    public class iniException : iCashError
    {
        public iniException()
            :base("0000008C")
        {}
    }
}

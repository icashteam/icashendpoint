﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  讀取錄碼 Key 失敗 => ??? 人工處理
    /// </summary>
    public class ReadKeyFails : iCashError
    {
        public ReadKeyFails()
            :base("00000111")
        {}
    }
}

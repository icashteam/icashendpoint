﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片累計－款總額超過上限 => Next
    /// </summary>
    public class iCashCardBonusLimitException : iCashError
    {
        public iCashCardBonusLimitException()
            :base("00000108")
        {}
    }
}

﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  icasH 卡片回應資料長度錯誤  => Next
    /// </summary>
    public class iCashResponseLengthError : iCashError
    {
        public iCashResponseLengthError()
            :base("0000020C")
        {}
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  Online 交易時，檢查電文 MAC 不正確 => Next
    /// </summary>
    public class OnlineMessageMACError : iCashError
    {
        public OnlineMessageMACError()
            :base("00000119")
        {}
    }
}

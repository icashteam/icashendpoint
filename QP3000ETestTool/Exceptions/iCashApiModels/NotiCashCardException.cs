﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  非 icash 卡(尋卡時回應) => Next
    /// </summary>
    public class NotiCashCardException : iCashError
    {
        public NotiCashCardException()
            :base("0000008B")
        {}
    }
}

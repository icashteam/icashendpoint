﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// icasH 中心端回應代碼錯誤 => Next
    /// </summary>
    public class iCashCenterResponseCodeError : iCashError
    {
        public iCashCenterResponseCodeError()
            :base("00000519")
        {}
    }
}

﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  PIN 碼認證失敗 => SetupReader 重新取得PIN CODE
    /// </summary>
    public class PinCodeError : iCashError
    {
        public PinCodeError()
            :base("00000114")
        {}
    }
}

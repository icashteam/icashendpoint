﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  授權代碼認證錯誤 => ???  先人工處理
    /// </summary>
    public class AuthCodeError : iCashError
    {
        public AuthCodeError()
            :base("00000113")
        {}
    }
}

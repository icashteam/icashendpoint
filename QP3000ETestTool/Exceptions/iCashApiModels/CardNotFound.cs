﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  找不到卡片 => Next
    /// </summary>
    public class CardNotFound : iCashError
    {
        public CardNotFound()
            :base("00000080")
        {}
    }
}

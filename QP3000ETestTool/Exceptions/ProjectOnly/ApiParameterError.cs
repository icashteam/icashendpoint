﻿using QP3000ETestTool.Business.Models;
using System;

namespace QP3000ETestTool.Exceptions.ProjectOnly
{
    public class ApiParameterError : Exception
    {
        public IResponse Response { get; set; }
    }
}

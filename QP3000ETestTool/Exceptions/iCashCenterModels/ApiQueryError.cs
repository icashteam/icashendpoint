﻿using System;

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///   一般通信 => 卡機 api 查詢失敗  => 下一次會更好
    ///  99 0009 
    /// </summary>
    public class ApiQueryError : iCashError
    {
        public ApiQueryError()
            :base("00990009")
        {}
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通信 => 此卡為黑名單 => 下一次會更好
    /// </summary>
    public class BlackListException : iCashError
    {
        public BlackListException()
            :base("00990003")
        {}
    }
}

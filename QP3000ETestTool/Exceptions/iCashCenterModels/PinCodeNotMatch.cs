﻿

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  開機認證(TermAuth) => 檢核配對錯誤(端末配對與端末開通不符合)  => SetupReader() 重新取得PIN CODE
    /// </summary>
    public class PinCodeNotMatch : iCashError
    {
        public PinCodeNotMatch()
            :base("00590007")
        {}
    }
}

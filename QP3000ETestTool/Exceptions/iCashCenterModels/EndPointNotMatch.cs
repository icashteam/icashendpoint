﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 端末配對錯誤  (POS 硬碟、機號和店號配對不正確 ) => 人工介入 
    /// </summary>
    public class EndPointNotMatch : iCashError
    {
        public EndPointNotMatch()
            :base("00990008")
        {}
    }
}

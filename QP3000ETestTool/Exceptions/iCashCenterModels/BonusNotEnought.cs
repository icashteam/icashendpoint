﻿using System;

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 交易金額錯誤 (負值) => 下次會更好 
    /// </summary>
    public class BonusNotEnought : iCashError
    {
        public BonusNotEnought()
            :base("00990013")
        {}
    }
}

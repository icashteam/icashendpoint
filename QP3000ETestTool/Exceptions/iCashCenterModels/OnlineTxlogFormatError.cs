﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// 一般通訊 => Txlog 資料錯誤  => 下次會更好 
    /// </summary>
    public class OnlineTxlogFormatError : iCashError
    {
        public OnlineTxlogFormatError()
            :base("00770003")
        {}
    }
}

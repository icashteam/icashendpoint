﻿using System;

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 無法授權 (聯名卡無法授權 ) => 下次會更好 
    /// </summary>
    public class CoBranderCardNotAuth : iCashError
    {
        public CoBranderCardNotAuth()
            :base("00990011")
        {}
    }
}

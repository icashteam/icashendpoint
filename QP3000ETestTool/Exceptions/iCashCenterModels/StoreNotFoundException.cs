﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  端末開通 SetupReader() => 中心端無該特店資訊   => 人工介入
    /// </summary>
    public class StoreNotFoundException1 : iCashError
    {
        public StoreNotFoundException1()
            :base("00890003")
        { }
    }

    /// <summary>
    /// 開機認證(TermAuth) => 中心端無該特店資訊 => 人工介入
    /// </summary>
    public class StoreNotFoundException2 : iCashError
    {
        public StoreNotFoundException2()
            : base("00590003")
        { }
    }

    /// <summary>
    ///  一般通訊 => 中心端無該特店資訊 => 人工介入
    /// </summary>
    public class StoreNotFoundException3 : iCashError
    {
        public StoreNotFoundException3()
            : base("00990004")
        { }
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// 一般通訊 => 特店不開放 加值  => 下次會更好 
    /// </summary>
    public class StoreFunctionNotAvailable : iCashError
    {
        public StoreFunctionNotAvailable()
            :base("00990010")
        {}
    }
}

﻿
namespace QP3000ETestTool.Exceptions
{
    /// 一般通訊 => 重複資料 (該筆 Online Txlog 為重複資料 ) => 下次會更好
    public class OnlineTxlogRepeat : iCashError
    {
        public OnlineTxlogRepeat()
            :base("00770002")
        {}
    }
}

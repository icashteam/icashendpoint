﻿using System;


namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// 一般通訊 => Ctag 失敗 => 下次會更好 
    /// </summary>
    public class CTagError : iCashError
    {
        public CTagError()
            :base("00990007")
        {}
    }
}

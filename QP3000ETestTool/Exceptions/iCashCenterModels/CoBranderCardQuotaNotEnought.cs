﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 配額次數超過限制 (聯名卡代行授權額度不足) => 下次會更好 
    /// </summary>
    public class CoBranderCardQuotaNotEnought : iCashError
    {
        public CoBranderCardQuotaNotEnought()
            :base("00990006")
        {}
    }
}

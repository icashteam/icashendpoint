﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  端末開通 SetupReader() => 授權碼取得失敗 ( 中心端產生授權碼失敗  )  => 人工介入
    /// </summary>
    public class PinCodeGetError : iCashError
    {
        public PinCodeGetError()
            :base("00890006")
        {}
    }
}

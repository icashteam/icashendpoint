﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 前台傳輸錯誤 (Timeout over 20 seconds)   => 下次會更好 
    /// </summary>
    public class CommonNetworkError : iCashError
    {
        public CommonNetworkError()
            :base("00190001")
        {}
    }

    /// <summary>
    ///  一般通訊 => 後台通訊錯誤   => 下次會更好 
    /// </summary>
    public class CommonNetworkError1 : iCashError
    {
        public CommonNetworkError1()
            : base("00990001")
        { }
    }

    /// <summary>
    ///  一般通訊 => 後台通訊錯誤   => 下次會更好 
    /// </summary>
    public class CommonNetworkError2 : iCashError
    {
        public CommonNetworkError2()
            : base("00770001")
        { }
    }

    /// <summary>
    ///  開機認證 TermAuth() => 後台通訊錯誤 => 等待10秒,再試一次
    /// </summary>
    public class SignOnNetworkError : iCashError
    {
        public SignOnNetworkError()
            : base("00590001")
        { }
    }

    /// <summary>
    ///  端末開通 SetupReader() => 後台通訊錯誤   => 等待60秒,再試一次
    /// </summary>
    public class SetupReaderNetworkError : iCashError
    {
        public SetupReaderNetworkError()
            : base("00890001")
        { }
    }
}

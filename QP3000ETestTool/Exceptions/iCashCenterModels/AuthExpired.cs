﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  通用 => 認證過期 => TermAuth() 重新認證
    /// </summary>
    public class AuthExpired : iCashError
    {
        public AuthExpired()
            :base("00190006")
        {}
    }
}

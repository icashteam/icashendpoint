﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///   通用 => 認證錯誤 => TermAuth() 重新認證
    /// </summary>
    public class CommonAuthError : iCashError
    {
        public CommonAuthError()
            :base("00190004")
        {}
    }

    /// <summary>
    ///  開機認證(TermAuth) => 授權認證失敗 => SetupReader() 重新取得PIN CODE
    /// </summary>
    public class SignOnAuthError : iCashError
    {
        public SignOnAuthError()
            : base("00590005")
        { }
    }
}

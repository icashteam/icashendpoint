﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    /// 一般通訊 => 自動加值功能未開啟 => 下次會更好 
    /// </summary>
    public class StoreFunctionNotEnable : iCashError
    {
        public StoreFunctionNotEnable()
            :base("00990014")
        {}
    }
}

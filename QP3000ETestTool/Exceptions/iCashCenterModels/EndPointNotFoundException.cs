﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  端末開通 SetupReader() => 查無端末  ( 中心端無該端末相關資訊  )  => 人工介入
    /// </summary>
    public class SetupReaderEndPointNotFound : iCashError
    {
        public SetupReaderEndPointNotFound()
            : base("00890002")
        { }
    }

    /// <summary>
    ///  開機認證 TermAuth() => 端末不符  ( 中心端無該端末相關資訊  )  => 人工介入
    /// </summary>
    public class TermAuthEndPointNotFound : iCashError
    {
        public TermAuthEndPointNotFound()
            : base("00590002")
        { }
    }

    /// <summary>
    ///  一般通訊 => 端末設置錯誤 ( 無該端末於特店設置記錄  )  => 人工介入
    /// </summary>
    public class CommonEndPointNotFound : iCashError
    {
        public CommonEndPointNotFound()
            : base("00990005")
        { }
    }
}

﻿using System;


namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 端末設置錯誤 ( 無該端末於特店設置記錄  )  => 人工介入
    /// </summary>
    public class EndPointDeprecate : iCashError
    {
        public EndPointDeprecate()
            :base("00890004")
        { }
    }
}

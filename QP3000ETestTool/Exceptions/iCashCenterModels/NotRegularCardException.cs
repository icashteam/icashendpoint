﻿using System;

namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 非有效卡  (POS 硬碟、機號和店號配對不正確 ) => 下次會更好 
    /// </summary>
    public class NotRegularCardException : iCashError
    {
        public NotRegularCardException()
            :base("00990012")
        {}
    }
}

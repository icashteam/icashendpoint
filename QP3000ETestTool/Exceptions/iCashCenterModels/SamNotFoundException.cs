﻿namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  端末開通 SetupReader() => 查無 SAM ( 中心端無該SAM 資訊  )  => 人工介入
    /// </summary>
    public class SamNotFoundException : iCashError
    {
        public SamNotFoundException()
            :base("00890005")
        {}
    }
}

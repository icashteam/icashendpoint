﻿
namespace QP3000ETestTool.Exceptions
{
    /// <summary>
    ///  一般通訊 => 電文格式錯誤  => 下次會更好 
    /// </summary>
    class MessageFormatError : iCashError
    {
        public MessageFormatError()
            :base("00190002")
        { }
    }
}

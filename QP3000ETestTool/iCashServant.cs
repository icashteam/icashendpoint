﻿using System.Runtime.InteropServices;

namespace QP3000ETestTool
{
    public static class iCashServant
    {
        //Import DLL
        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint GetApVersion([Out] byte[] PosApiVersion, [Out] byte[] IcashVersion);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint SetupReader([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [Out] byte[] Pin, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint TermAuth([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [In] byte[] Pin, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        
        public static extern uint QueryCardInfo([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [Out] byte[] RespData);
        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint TradeCharge([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [In] int TradeAmount, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint TradeChargeCancel([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [In] int TradeAmount, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint TradeSale([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [In] int TradeAmount, [Out] byte[] AutoloadAmount, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint TradeRefund([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [In] int TradeAmount, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint Settlement([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint TradeAutoLoad([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [In] int TradeAmount, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint LockCard([In] byte[] MarketCode, [In] byte[] StoreNo, [In] byte[] PosCode, [In] byte[] TradeSn, [In] byte[] CashierCode, [Out] byte[] RespData);

        [DllImport("Icash2Api.dll", CharSet = CharSet.Ansi)]
        public static extern uint UpdateFW();
    }
}

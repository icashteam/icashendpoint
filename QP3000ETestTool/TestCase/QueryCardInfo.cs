﻿using System;


namespace QP3000ETestTool.TestCase
{
    public static class QueryCardInfo
    {
        public static string Test()
        {
            byte[] baResponse = new byte[1024];
            uint irc = iCashServant.QueryCardInfo( StoreInfo.MarketCode , StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, baResponse);
            if (irc == 0)
            {
                byte[] CardNO = new byte[16];
                byte[] Balance = new byte[8];
                byte[] CardType = new byte[2];
                byte[] expiredDate = new byte[8];
                byte[] cardStatus = new byte[2];
                byte[] ipointFlag = new byte[2];
                byte[] lastTxlog = new byte[25];

                string result = "";
                Array.Copy(baResponse, 9, CardNO, 0, 16);
                string strCardNO = System.Text.Encoding.Default.GetString(CardNO);
                result += "卡號:" + strCardNO + "\r\n";
                Array.Copy(baResponse, 25, Balance, 0, 8);
                string strBalance = System.Text.Encoding.Default.GetString(Balance);
                result += "餘額:" + strBalance + "\r\n";
                Array.Copy(baResponse, 33, CardType, 0, 2);
                string strCardType = System.Text.Encoding.Default.GetString(CardType);
                result += "卡別:" + strCardType + "\r\n";
                Array.Copy(baResponse, 35, expiredDate, 0, 8);
                string strExpiredDate = System.Text.Encoding.Default.GetString(expiredDate);
                result += "有效期限:" + strExpiredDate + "\r\n";
                Array.Copy(baResponse, 43, cardStatus, 0, 2);
                string strCardStatus = System.Text.Encoding.Default.GetString(cardStatus);
                result += "卡片狀態:" + strCardStatus + "\r\n";
                Array.Copy(baResponse, 45, ipointFlag, 0, 2);
                string stripointFlag = System.Text.Encoding.Default.GetString(ipointFlag);
                result += "累點註記:" + stripointFlag + "\r\n";
                Array.Copy(baResponse, 47, lastTxlog, 0, 24);
                string strLastTxlog = System.Text.Encoding.Default.GetString(lastTxlog);
                result += "最後一筆交易:" + strLastTxlog + "\r\n";
                return result;
            }


            return "Query irc : " + irc.ToString("X8");
        }
    }
}

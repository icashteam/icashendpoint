﻿using System;
using System.Text;

namespace QP3000ETestTool.TestCase
{
    public static class SetupReader
    {
        public static string Test()
        {
            byte[] baResponse = new byte[1024];
            byte[] SysPIN = new byte[4];

            uint irc = iCashServant.SetupReader(StoreInfo.MarketCode , StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, SysPIN, baResponse);
            if (irc == 0)
            {
                return Encoding.Default.GetString(SysPIN);
            }
            else
            {
                throw new Exception( "Setup Reader Fail\nRC = " + irc.ToString("X8") );
            }
        }
    }
}

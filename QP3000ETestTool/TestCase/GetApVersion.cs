﻿using System;
using System.Linq;

namespace QP3000ETestTool.TestCase
{
    public static class GetApVersion
    {
        public static string Test()
        {
            string result = null;
            byte[] bytPOSAPIVer = new byte[14];
            byte[] bytiCashVer = new byte[4];

            if ( iCashServant.GetApVersion( bytPOSAPIVer, bytiCashVer) == 0 )
            {
                result    = bytPOSAPIVer.Select(Convert.ToChar).Aggregate( "API Ver: \n", (a,b) => a + b ) + "\n";
                result += bytiCashVer.Select(Convert.ToChar).Aggregate( "iCash Ver: \n", (a, b) => a + b) + "\n";
            }

            return result;
        }
    }
}


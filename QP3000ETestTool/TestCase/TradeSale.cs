﻿using System;
using System.Text;

namespace QP3000ETestTool.TestCase
{
    public static class TradeSale
    {
        public static string Test(int cost)
        {
            byte[] baResponse = new byte[1024];
            byte[] baAutoloadAmount = new byte[9];

            uint irc = iCashServant.TradeSale(StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, cost, baAutoloadAmount, baResponse);
            if (irc == 0)
            {
                string result = string.Empty;
                result += "Trade Sale irc:" + irc.ToString("X8") + "\r\n";
                byte[] baBal = new byte[8];
                Array.Copy(baResponse, 25, baBal, 0, 8);
                result += "Trade Sale 交易前餘額 :" + Encoding.Default.GetString(baBal) + "\r\n";
                result += "Trade Sale 交易金額 :" + cost + "\r\n";
                Array.Copy(baResponse, 33, baBal, 0, 8);
                result += "Trade Sale 交易後餘額 :" + Encoding.Default.GetString(baBal) + "\r\n";
                result += "AutoLoad Charge :" + Encoding.Default.GetString(baAutoloadAmount) + "\r\n";

                return result;
            }

            return "Trade Sale irc:" + irc.ToString("X8") + "\r\n";
        }
    }
}

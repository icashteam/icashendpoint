﻿using System;

namespace QP3000ETestTool.TestCase
{
    public static class TradeChargeCancel
    {
        public static string Test(int cost)
        {
            byte[] baResponse = new byte[1024];
            
            int irc = (int)iCashServant.TradeChargeCancel(StoreInfo.MarketCode, StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, cost, baResponse);
            if( irc == 0 )
            {
                string result = string.Empty;
                result += "Trade Charge Cancel irc:" + irc.ToString("X8") + "\r\n";
                byte[] baBal = new byte[8];
                Array.Copy(baResponse, 25, baBal, 0, 8);
                result += "Trade Charge 交易前餘額 :" + System.Text.Encoding.Default.GetString(baBal) + "\r\n";
                result += "Trade Charge 交易金額 :" + cost + "\r\n";
                Array.Copy(baResponse, 33, baBal, 0, 8);
                result += "Trade Charge 交易後餘額 :" + System.Text.Encoding.Default.GetString(baBal) + "\r\n";

                return result;
            }
            
            return "Trade Charge Cancel irc:" + irc.ToString("X8") + "\r\n";
        }
    }
}

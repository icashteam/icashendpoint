﻿using System.Text;


namespace QP3000ETestTool.TestCase
{
    public static class TermAuth
    {
        public static string Test(string pinCode)
        {
            byte[] SysPIN = Encoding.Default.GetBytes(pinCode);

            byte[] baResponse = new byte[1024];

            uint irc = iCashServant.TermAuth(StoreInfo.MarketCode , StoreInfo.StoreNO, StoreInfo.PosCode, StoreInfo.TradeSn, StoreInfo.CashierCode, SysPIN, baResponse);
            if (irc == 0)
            {
                return "Terminal Auth OK";
            }
            else
            {
                return "Terminal Auth Fail\nRC = " + irc.ToString("X8");
            }
        }
    }
}

﻿using System.Text;

namespace QP3000ETestTool
{
    public static class StoreInfo
    {
        //特約機構代號 
        public static byte[] MarketCode
        {
            get => Encoding.Default.GetBytes("W01");
        }

        // 店號
        public static byte[] StoreNO
        {
            get => Encoding.Default.GetBytes("82802149");
        }

        // POS 機號
        public static byte[] PosCode
        {
            get => new byte[] { 0x30, 0x30, 0x31 };
        }

        // 收銀機編號
        public static byte[] CashierCode
        {
            get => new byte[] { 0x30, 0x30, 0x30, 0x30 };
        }

        // 交易序號
        public static byte[] TradeSn
        {
            get => new byte[] { 0x30, 0x30, 0x30, 0x30, 0x30, 0x31 };
        }
    }
}

﻿using System;
using System.Text;

namespace BlackLIstParser.Business.Models
{
    public class BHeader
    {
        /// <summary>
        ///  BHeader 長度固定
        /// </summary>
        public const int Length = 40;

        /// <summary>
        ///  裸資料
        /// </summary>
        private byte[] raw = new byte[Length];

        /// <summary>
        ///  new
        /// </summary>
        public BHeader(byte[] data)
        {
            Array.Copy(data, BLabel.Length, raw, 0, Length);
        }

        public string DataID
        {
            get
            {
                return "0x" + raw[0].ToString("X2");
            }
        }

        public string TimeStamp
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int index = 1; index < 8; ++index)
                    result.Append(Helper.CompressBCD(raw[index]));

                return result.ToString();
            }
        }
        public string Total
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int index = 8; index < 11; ++index)
                    result.Append(Helper.CompressBCD(raw[index]));

                return result.ToString();
            }
        }
        public string Modify
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int index = 11; index < 14; ++index)
                    result.Append(Helper.CompressBCD(raw[index]));

                return result.ToString();
            }
        }

        public string SHA1
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int index = 14; index < 34; ++index)
                    result.Append(raw[index].ToString("X2"));

                return result.ToString();
            }
        }

        public byte[] SHA1raw
        {
            get
            {
                byte[] result = new byte[20];

                Array.Copy(raw, 14, result, 0, 20);
                return result;
            }
        }

        public string Padding
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int index = 34; index < 40; ++index)
                    result.Append(raw[index].ToString("X2"));

                return result.ToString();
            }
        }
    }
}

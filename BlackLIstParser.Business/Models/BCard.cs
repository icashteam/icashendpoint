﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using BlackLIstParser.Business.Exceptions;

namespace BlackLIstParser.Business.Models
{
    public class BCard
    {
        /// <summary>
        ///  BCard 長度固定 8
        /// </summary>
        public const int Length = 8;

        /// <summary>
        ///  new
        /// </summary>
        public BCard(byte[] data, int index)
        {
            StringBuilder result = new StringBuilder();
            for (int offset = index; offset < index + Length; ++offset)
                result.Append(Helper.CompressBCD(data[offset]));

            CardNo = result.ToString();
        }

        public string CardNo { get; private set; }
    }

    public class BCardBundle
    {
        private const int StartIndex = BLabel.Length + BHeader.Length;
        private byte[] source;

        /// <summary>
        ///  new
        /// </summary>
        /// <param name="data"></param>
        public BCardBundle(byte[] data)
        {
            Precondition(data);

            source = new byte[data.Length - StartIndex];//將字串轉為Byte[]
            Array.Copy(data, StartIndex, source, 0, source.Length);
        }

        public List<BCard> Cards
        {
            get
            {
                return GenerateCards().ToList();
            }
        }

        public byte[] SHA1raw
        {
            get
            {
                SHA1 sha1 = new SHA1CryptoServiceProvider();//建立一個SHA1

                return sha1.ComputeHash(source);
            }
        }

        public string SHA1
        {
            get
            {
                StringBuilder result = new StringBuilder();
                for (int index = 0; index < SHA1raw.Length; ++index)
                    result.Append(SHA1raw[index].ToString("X2"));

                return result.ToString();
            }
        }

        /// <summary>
        ///  必須為八的倍數
        /// </summary>
        private static void Precondition(byte[] data)
        {
            if ((data.Length - StartIndex) % BCard.Length != 0)
                throw new BCardsChunksLengthError { };
        }

        private IEnumerable<BCard> GenerateCards()
        {
            for (int index = 0; index < source.Length; index += BCard.Length)
                yield return new BCard(source, index);
        }
    }
}

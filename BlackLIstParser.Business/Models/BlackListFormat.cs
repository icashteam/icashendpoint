﻿using System.Linq;
using BlackLIstParser.Business.Exceptions;


namespace BlackLIstParser.Business.Models
{
    public class BlackListFormat
    {
        public BLabel Label { get; private set; }
        public BHeader Header { get; private set; }
        public BCardBundle CardBundle { get; private set; }

        public BlackListFormat(byte[] raw)
        {
            PreCondition(raw);
            {
                Label = new BLabel(raw);
                Header = new BHeader(raw);
                CardBundle = new BCardBundle(raw);
            }
            PostCondition();
        }

        private void PreCondition(byte[] raw)
        {
            if (raw.Length < (BLabel.Length + BHeader.Length))
                throw new BlackListTooShort { };
        }

        private void PostCondition()
        {
            if( Header.SHA1raw.SequenceEqual(CardBundle.SHA1raw) == false )
                throw new BlackListChecksumError { };
        }
    }
}

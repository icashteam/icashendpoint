﻿using System;
using System.Text;

namespace BlackLIstParser.Business.Models
{
    public class BLabel
    {
        /// <summary>
        ///  Label 長度固定
        /// </summary>
        public const int Length = 128;

        /// <summary>
        ///  裸資料
        /// </summary>
        private byte[] raw = new byte[Length];

        /// <summary>
        ///  new
        /// </summary>
        public BLabel(byte[] data)
        {
            Array.Copy(data, 0, raw, 0, Length);
        }

        public string HeaderCode
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 0, 3);
            }
        }

        public char Info
        {
            get
            {
                return (char)raw[0 + 3];
            }
        }

        public string StoreCode
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 4, 19).Trim();
            }
        }

        public string ManufacturerID
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 23, 20).Replace(' ', '*');
            }
        }

        public string DomainID
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 43, 4);
            }
        }

        public string SP_ID
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 47, 3);
            }
        }

        public string ManufacturerTime
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 50, 14);
            }
        }

        public char DataType
        {
            get
            {
                return (char)raw[64];
            }
        }
        public string CommandType
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 65, 2);
            }
        }
        public string TokenID
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 67, 9).Replace(' ', '*');
            }
        }
        public char EncryptFlag
        {
            get
            {
                return (char)raw[76];
            }
        }
        public string OriginDataLength
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 77, 6);
            }
        }
        public string PreEncryptDataLength
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 83, 6);
            }
        }
        public char ContinueFlag
        {
            get
            {
                return (char)raw[89];
            }
        }
        public char CutSerial
        {
            get
            {
                return (char)raw[90];
            }
        }
        public string Version
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 91, 4).Replace(' ', '*');
            }
        }
        public string AP_ID
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 95, 4);
            }
        }
        public string Padding
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 99, 20).Replace(' ', '*');
            }
        }
        public string BuddleSize
        {
            get
            {
                return Encoding.ASCII.GetString(raw, 119, 9);
            }
        }
    }

}

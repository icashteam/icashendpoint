﻿
namespace BlackLIstParser.Business
{
    public static class Helper
    {
        public static string CompressBCD(byte bcd)
        {
            uint hight = (uint)bcd >> 4;
            uint low = (uint)bcd & (uint)0x0F;

            return hight.ToString() + low.ToString();
        }
    }
}

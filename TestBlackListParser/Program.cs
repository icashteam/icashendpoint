﻿using BlackLIstParser.Business.Models;
using System;
using System.IO;
using System.Linq;

namespace TestBlackListParser
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("CicashH Length: " + File.ReadAllBytes("CicasH.dat").Length);
            Console.WriteLine("LicashH Length: "  + File.ReadAllBytes("LicasH.dat").Length);

            BlackListFormat bt = new BlackListFormat( File.ReadAllBytes("LicasH.dat") );
            Console.WriteLine("bt.Label.HeaderCode: " + bt.Label.HeaderCode + "---" );
            Console.WriteLine("bt.Label.Info: " + bt.Label.Info + "---");
            Console.WriteLine("bt.Label.StoreCode: " + bt.Label.StoreCode + "---");
            Console.WriteLine("bt.Label.ManufacturerID: " + bt.Label.ManufacturerID + "---");
            Console.WriteLine("bt.Label.DomainID: " + bt.Label.DomainID + "---");
            Console.WriteLine("bt.Label.SP_ID: " + bt.Label.SP_ID + "---");
            Console.WriteLine("bt.Label.ManufacturerTime: " + bt.Label.ManufacturerTime + "---");
            Console.WriteLine("bt.Label.DataType: " + bt.Label.DataType + "---");
            Console.WriteLine("bt.Label.CommandType: " + bt.Label.CommandType + "---");
            Console.WriteLine("bt.Label.TokenID: " + bt.Label.TokenID + "---");
            Console.WriteLine("bt.Label.EncryptFlag: " + bt.Label.EncryptFlag + "---");
            Console.WriteLine("bt.Label.OriginDataLength: " + bt.Label.OriginDataLength + "---");
            Console.WriteLine("bt.Label.PreEncryptDataLength: " + bt.Label.PreEncryptDataLength + "---");
            Console.WriteLine("bt.Label.ContinueFlag: " + bt.Label.ContinueFlag + "---");
            Console.WriteLine("bt.Label.CutSerial: " + bt.Label.CutSerial + "---");
            Console.WriteLine("bt.Label.Version: " + bt.Label.Version + "---");
            Console.WriteLine("bt.Label.AP_ID: " + bt.Label.AP_ID + "---");
            Console.WriteLine("bt.Label.Padding: " + bt.Label.Padding + "---");
            Console.WriteLine("bt.Label.BuddleSize: " + bt.Label.BuddleSize + "---");

            Console.WriteLine("bt.Header.DataID: " + bt.Header.DataID + "---");
            Console.WriteLine("bt.Header.TimeStamp: " + bt.Header.TimeStamp + "---");
            Console.WriteLine("bt.Header.Total: " + bt.Header.Total + "---");
            Console.WriteLine("bt.Header.SHA1: " + bt.Header.Modify + "---");
            Console.WriteLine("bt.Header.Checksum: " + bt.Header.SHA1 + "---");
            Console.WriteLine("bt.Header.Padding: " + bt.Header.Padding + "---");

            Console.WriteLine("bt.CardBundle.SHA1: " + bt.CardBundle.SHA1 + "---");
            Console.WriteLine("bt.CardBundle.Cards.First().CardNo: " + bt.CardBundle.Cards.First().CardNo + "---");
            Console.WriteLine("bt.CardBundle.Cards.Last().CardNo: " + bt.CardBundle.Cards.Last().CardNo + "---");
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using QP3000ETestTool.Business;
using QP3000ETestTool.Business.Models;
using QP3000ETestTool.Exceptions;
using QP3000ETestTool.Exceptions.ProjectOnly;
using QP3000ETestTool.Interface;
using QP3000ETestTool.Models.Parameter;

namespace iCashConsole
{
    class Program : EndPointBusiness
    {
        private const string PersistentDirectory = "C:/Users/Public/iCash";
        private readonly string PinFile = $"{PersistentDirectory}/pin.txt";
        private readonly string BlackListFile = $"{PersistentDirectory}/blacklist.dat";

        static void Main(string[] args)
        {
            new Program().CardReaderProcedure();
        }

        protected override void Precondition()
        {
            if (Directory.Exists(PersistentDirectory) == false)
                throw new PersistenNotExist { };
        }

        protected override string ReadPingCode()
        {
            if (File.Exists(PinFile) == false)
                throw new PinCodeNotExistException { };

            string result = File.ReadAllText(PinFile);
            if (string.IsNullOrWhiteSpace(result))
                throw new PinCodeNotExistException { };
            else
                return result;
        }

        protected override void UpdatePingCode(string value)
        {
            File.WriteAllText(PinFile, value);
        }

        protected override IRequest GetRequest()
        {
            Console.WriteLine("1: 查詢");
            Console.WriteLine("2: 扣款");
            Console.WriteLine("3: 加值");
            Console.WriteLine("4: 取消加值");
            Console.WriteLine("5: 退貨加值");

            return new TestRequest
            {
                Command = Console.ReadLine(),
                Parameter = new TradeParameter
                {
                    Name = "測試刷卡",
                    Amount = 20,
                    Cookie = "客戶端儲存區"
                }
            };
        }

        protected override void StoreBlackList( byte[] raw)
        {

        }

        protected override byte[] LoadBlackList()
        {
            return new byte[] { };
        }

        protected override void Ping()
        {

        }

        protected override IStoreInfo NextTradeConfig()
        {
            throw new NotImplementedException();
        }
    }

    class TestRequest : IRequest
    {
        public IResponse Response { get; set; } = new TestResponse { };
        public string Command { get; set; }
        public object Parameter { get; set; }
    }

    class TestResponse : IResponse
    {
        public void OK(object result)
        {
            Console.WriteLine( JsonConvert.SerializeObject(result) );
        }

        public void Error(string errorCode, string ErrorMessage)
        {
            Console.WriteLine(errorCode + " : " + ErrorMessage);
        }
    }
}
